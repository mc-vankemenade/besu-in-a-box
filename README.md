# Besu In A Box: Getting started
Within the Ethereum Development sphere there are dozens of tools, frameworks IDE's and plugins to make developing Smart-contracts as easy as possible. However, most of these tools are focused on testing and Quality-of-life when writing code. As opposed to a persistent environment that can be tested on and shared. This is why I created Besu-in-a-box. An easy way to create a persistent ethereum network for development and testing.

## Design choices
### Why besu?
The choice to use Hyperleder Besu was made based on the availability of alternate consensus mechanisms along with them offering a container image. This made it rather easy to spin up without causing clutter on the developers machine. Along with less resource usage since the network could be run using Proof-of-authority. The combination of these factors makes Besu ideal for a portable network.

### Why Proof-of-Authority?
Since the consensus algorithm has no direct effect on the execution of smart-contracts I opted to choose the lightest algorithm possible. This ended up being Proof-of-authority. This algorithm isn't as secure as POW or POS however, since the nodes are all hosted on one secure machine this matters very little. 

### Why Docker?
Since Hyperledger offers container images of Besu the easiest way to make a portable system would be to use these containers. This reduces any file clutter and dependencies usually needed in order to run multiple ethereum nodes. The choice for docker was also made fairly quickly due to its easy "compose" syntax and automated management. 

### Further automation
After setting up all of the infrastructure mentioned above I still found the process fairly complex. generating the different keys and setting up the genesis file requires multiple steps that can easily mess up the stack of nodes. In order to make the management process easier i wanted to create an automated script for completing all these steps.

Enter, Besu.py. A minimal python script that handles the creation of the keys and genesis file. It also allows the user to customise their blockchain and prefund any account for easier testing purposes. Along with these settings I added a simple start/stop option to forgo the more complex commands.

## Getting set up
Now that you know why I did what. Lets see what you'll need to spin up your own blockchain.
- Docker
- Docker-compose
- Python 3.10 or newer
- x86_64 processor architecture

The 64 bit architecture might seem like weird requirement. However since we're going to start multiple Ethereum-Virtual-Machines we need that specific architecture.

Once your set up and docker works as expected move on to the next steps.

## Clone the project
First of all, we need to clone the repository. Open up a command prompt, move to wherever you want to store the project and enter the following:
```
git clone https://gitlab.com/mc-vankemenade/besu-in-a-box.git
```

## Running the Python script
In order to start the managemen script enter the following command once you are insite the project directory
```
python3 ./besu.py
```
From here you will see a prompt with an introduction and 4 choices.

>1. Generate the configuration files
>2. Start the blockchain network
>3. Stop the blockchain network
>4. Exit

Option 2/3/4 are fairly self explanatory so lets dive into the different settings for option 1.

### ChainID
the chainID of a blockchain dictates the main ID used by all nodes on the network. This can be any number, however some numbers can be in use by production networks. For example number 1 is in use by the Ethereum mainnet. And 5 is used by the Goerli testnet. Any number above 100 generally is free to use without any problems

### BlockTime
The blocktime indicates the minimum time between each block. Generally you'll want to balance this with the amount of storage you have and responsiveness you need. A lower block time will generally process transactions faster. However it will fill up your hard drive rather quickly since it's constantly generating data. This also has an effect on the resource usage of the blockchain. If you use a higher blocktime eg, 10 seconds. Your computer has less processing to do.

### Gaslimit
The gaslimit determines the maximum amount of gas that can fit in a single block. Effectively limiting the complexity of a contract. By default this is set to the maximum value since we are creating a free gas network.

### Prefunding accounts
If you want to test your contracts you'll generally want some ETH to play around with. This is what prefunding sets up. When you fill in your address the genesis file will allocate 10 Million ETH to your account. And since gas is free, you won't lose anything by transferring it.

## Next steps
After you've filled in these steps you will be returned to the first menu. There you can choose the second option to start the blockchain network.

## Monitoring
In addition to the 4 blockchain nodes there is also a Grafana dashboard running to show metrics about each node. This dashboard can be reached at:
```
http://localhost:3000
```
The default login is _admin_ for both the username and password.

## Connecting metamask
When the network is running Node-1 is reachable at the following address:
```
http://127.0.0.1:8545
```
Whenever you connect with metamask be sure to use the chainID used when generating the genesis file.
___
## Manually Modifying parameters

### ```/setupFiles/templateGenesis.json```

This file is the template for generating the main `genesis.json` file. Usually it gets generated by the script from `templateGenesis.default.json`. 

>_modifying this file has no effect on parameters due to it being overwritten whenever new keys are generated_

### `/setupFiles/genesis.json`

This is the genesis file for each node on the network. It declares the settings of the blockchain such as well as pre-funded accounts and the consensus mechanism used by the nodes.

> _If you want to modify the settings in this file you have to clear the Database file stored in each nodes folder. This is due to the tamper proof nature of the blockchain._

### Prefunding accounts

If you want to pre-fund some accounts you can do so by modifying the "alloc" section of the genesis file. Simply copy the account address of the account you want to fund and paste it without the "0x" prefix in the file. It should look something like this:

```json
"alloc" : {
    "815e4B005Bf2601A886756d8da7A508E526E5AFa": {
      "balance":"1000000000000000000000000"
    }
  },
```

### Other variables

The meaning and use of the other variables in the genesis file can be found here:

(**disclaimer!** This is not the Official Besu documentation so some commands might not work outside of the genesis file.)

[Private Networks](https://geth.ethereum.org/docs/interface/private-network)

The original documentation for hyperledger Besu can be found here:

[Hyperledger Besu Enterprise Ethereum client - Hyperledger Besu](https://besu.hyperledger.org/en/stable/)

### /{node-name}/config.toml

This config file sets the environment variables for each node. The reference for this file can be found here:

[Options - Hyperledger Besu](https://besu.hyperledger.org/en/stable/Reference/CLI/CLI-Syntax/)

The reference initially displays the settings in command format. However you can select the *example configuration file* setting above the command to show it in config file format.