import subprocess
import os
import json

# Show the menu
def printMenu():
    print("Hi, welcome to the Hyperledger besu controller!")
    print("Please select one of the options below.")
    print("1. Run node setup.")
    print("2. Start the blockchain network.")
    print("3. Stop the blockchain network.")
    print("4. Exit.")
    print("\n")


# Function for setting up the required files
def runSetup():
    # Get the current working directory
    cwd = os.getcwd()

    # Load the template json file
    print(" - Template Genesis loaded.")
    with open(cwd + '/setupFiles/templateGenesis.default.json', 'r') as f:
        templateGenesis = json.load(f)

    print(templateGenesis)
    print("\nThe next couple prompts will setup the network according to your preferences")
    print("Leaving them blank will start a free gas network with chainID 12345\n")

    # Request chainID otherwise use default values
    val = input("Enter ChainID (default: 12345): ")
    if bool(val):
        templateGenesis['genesis']['config']['chainId'] = int(val)
    else:
        templateGenesis['genesis']['config']['chainId'] = 12345

    # Request blockTime otherwise use default values
    val = input("Enter BlockTime (default: 2): ")
    if bool(val):
        templateGenesis['genesis']['config']['ibft2']['blockperiodseconds'] = int(val)
        templateGenesis['genesis']['config']['ibft2']['requesttimeoutseconds'] = int(val) * 2
    else:
        templateGenesis['genesis']['config']['ibft2']['blockperiodseconds'] = 2
        templateGenesis['genesis']['config']['ibft2']['requesttimeoutseconds'] = 4

    # Request gasLimit otherwise use default values
    val = input("Enter GasLimit (default: 0x1fffffffffffff): ")
    if bool(val):
        templateGenesis['genesis']['gasLimit'] = str(val)
    else:
        templateGenesis['genesis']['gasLimit'] = '0x1ffffffffffff'

    # Request to prefund account otherwise use default values
    val = input("Do you want to prefund an account? Y/N (default: N): ")
    if val == 'y' or val == 'Y':
        val = input("Enter the account address (eg: 0x122334...): ")
        if bool(val):
            templateGenesis['genesis']['alloc'][str(val)[2:]] = {'balance' : "1000000000000000000000000" }

    # write JSON back to templateGenesis.json
    with open(cwd + '/setupFiles/templateGenesis.json', 'w') as json_file:
        json.dump(templateGenesis, json_file)
        
    print("\nAll done. Generating config files...\n")

    print(" - Checking for old keys...")
    # See if there's anything in the networkfiles folder
    keyFiles = subprocess.check_output(['ls', cwd + '/networkFiles'])

    # Clear old keys and genesis file
    if bool(keyFiles.decode('utf8')) == True:
        print(' - Clearing old keys!')
        subprocess.run(['rm', '-r', cwd + '/networkFiles/keys'])
        subprocess.run(['rm', '-r', cwd + '/networkFiles/genesis.json'])

    # Run the docker script to have one node generate the new keys
    initialSetup = subprocess.run(['docker-compose', '-f', cwd + '/docker-compose.setup.yml', 'up'])

    # Command error handling
    if (initialSetup.returncode != 0):
        print(" - There was a problem generating the node keys, Check the output above!")
        return

    # Read the generated keys and slice them into an array
    keyFiles = subprocess.check_output(['ls', cwd + '/networkFiles/keys'])
    keyFiles = keyFiles.decode('utf-8').split('\n')
    keyFiles.pop()
    
    # Copy the keyfiles to each node
    nodes = ['Node-1', 'Node-2', 'Node-3', 'Node-4']
    i = 0
    for x in nodes:
        subprocess.run(['rm', '-r', cwd + '/' + x + '/caches'])
        subprocess.run(['rm', '-r', cwd + '/' + x + '/database'])
        subprocess.run(['rm', '-r', cwd + '/' + x + '/DATABASE_METADATA.json'])

        subprocess.run(['cp', cwd + '/networkFiles/keys/' + keyFiles[i] + '/key', cwd + '/' + x])
        subprocess.run(['cp', cwd + '/networkFiles/keys/' + keyFiles[i] + '/key.pub', cwd + '/' + x])
        print("\nCopied keys for " + x + "\n")
        i = i + 1

    # Copy the generated Genesis file to the setupfiles folder for each node
    subprocess.run(['cp', cwd + '/networkFiles/genesis.json', cwd + '/setupFiles/genesis.json'])
    print("\nSuccessfully copied Genesis file to nodes!\n")


def startStack():
    subprocess.run(['docker-compose', 'up', '-d'])

def stopStack():
    subprocess.run(['docker-compose', 'down'])

# Main loop
while(True):
    printMenu()
    choice = int(input("Enter your choice: "))

    match(choice):
        case 1:
            runSetup()
        
        case 2:
            startStack()

        case 3:
            stopStack();
        
        case 4:
            break
        
